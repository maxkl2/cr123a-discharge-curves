# CR123A Discharge Curve

This repo contains discharge curves of CR123A batteries extracted from multiple datasheets.

![Battery Voltage over Discharge %](plot_v_vs_percent.png)

The Python Code also contains an attempted mapping of voltages to battery percentage. Due to large temperature variation it also includes temperature compensation.

![Reported vs Actual Discharge %](plot_percentage_mapping.png)
