
from pathlib import Path
import enum
from enum import Enum
from dataclasses import dataclass
from typing import Tuple

import matplotlib.pyplot as plt
import numpy as np


@dataclass
class Config:
    filename: str
    range_x: Tuple[float, float]
    range_y: Tuple[float, float]
    temp: float
    note_text: str


# https://www.tme.eu/Document/4dfa436d3d693704b7dd8b1095903d70/BAT-CR123A.pdf
config_GP_30mA = Config(
    filename = 'BAT-CR123A/path_30mA_23C.txt',
    range_x = (0, 100),
    range_y = (2, 3.25),
    temp = 23,
    note_text = 'at 30 mA, 23 °C (GP)',
)

# https://ind.gpbatteries.com/pub/media/uploads/pdf/primary_lithium/cylindrical/datasheet/GPCR123A.pdf
config_GP_10mA = Config(
    filename = 'GPCR123A/path_10mA_23C.txt',
    range_x = (0, 100),
    range_y = (2, 3.24),
    temp = 23,
    note_text = 'at 10 mA, 23 °C (GP)',
)

# https://biz.maxell.com/en/primary_batteries/CyCR_22e.pdf
config_Maxell_5mA = Config(
    filename = 'CyCR_22e/path_5mA_20C.txt',
    range_x = (0, 100),
    range_y = (2, 3.2),
    temp = 20,
    note_text = 'at 5 mA, 20 °C (Maxell)',
)
config_Maxell_40mA = Config(
    filename = 'CyCR_22e/path_40mA_20C.txt',
    range_x = (0, 100),
    range_y = (2, 3.18),
    temp = 20,
    note_text = 'at 40 mA, 20 °C (Maxell)',
)
config_Maxell_40mA__10C = Config(
    filename = 'CyCR_22e/path_40mA_-10C.txt',
    range_x = (0, 100),
    range_y = (2, 3.26),
    temp = -10,
    note_text = 'at 40 mA, -10 °C (Maxell)',
)
config_Maxell_40mA_0C = Config(
    filename = 'CyCR_22e/path_40mA_0C.txt',
    range_x = (0, 100),
    range_y = (2, 3.25),
    temp = 0,
    note_text = 'at 40 mA, 0 °C (Maxell)',
)
config_Maxell_40mA_60C = Config(
    filename = 'CyCR_22e/path_40mA_60C.txt',
    range_x = (0, 100),
    range_y = (2, 3.22),
    temp = 60,
    note_text = 'at 40 mA, 60 °C (Maxell)',
)

# mapping = [
#     (2.0 ,   0),
#     (2.5 ,  10),
#     (2.65,  20),
#     (2.7 ,  30),
#     (2.8 ,  40),
#     (2.85,  50),
#     (2.9 ,  80),
#     (3.0,   90),
#     (3.1,   95),
#     (3.2 , 100),
# ]

# mapping = [
#     (2.0 ,   0),
#     (2.6 ,  10),
#     (2.65,  20),
#     (2.7 ,  30),
#     (2.8 ,  40),
#     (2.85,  50),
#     (2.9,  80),
#     (3.0,   90),
#     (3.05,  94),
#     (3.1,   95),
#     (3.19,  98),
#     (3.2 , 100),
# ]

# mapping = [
#     (2.0 ,   0),
#     (2.1 ,   1),
#     (2.2 ,   3),
#     (2.3 ,   4),
#     (2.4 ,   7),
#     (2.5 ,  10),
#     (2.6 ,  15),
#     (2.65,  18),
#     (2.7 ,  22),
#     (2.75,  27),
#     (2.8 ,  32),
#     (2.85,  40),
#     (2.9 ,  63),
#     (2.95,  99),
#     (3.0,   99.5),
#     (3.05,  99.8),
#     (3.1,   99.9),
#     # (3.19,  98),
#     (3.2 , 100),
# ]

# Room temperature (20 °C)
mapping_20C = [
    # (V, %)
    (2.0 ,   0),
    (2.5 ,  10),
    (2.6,  15),
    # (2.65,  18),
    (2.7 ,  22),
    (2.8 ,  32),
    (2.85,  40),
    (2.9 ,  63),
    (3.0,   90),
    (3.1,   95),
    (3.2 , 100),
]

# Low temperature (0 °C)
mapping_0C = [
    # (V, %)
    (2.0 ,   0),
    (2.3 ,   9),
    (2.5 ,  25),
    (2.6 ,  48),
    # (2.65,  18),
    (2.7 ,  97),
    # (2.8 ,  32),
    # (2.85,  40),
    # (2.9 ,  63),
    # (3.0,   90),
    (2.8,   99),
    (3.2 , 100),
]

# High temperature (60 °C)
mapping_60C = [
    # (V, %)
    (2.0 ,   0),
    (2.5 ,  3),
    (2.6,  4),
    # (2.65,  18),
    (2.7 ,  10),
    (2.8 ,  21),
    (2.85,  29),
    (2.9 ,  41),
    (3.0,   99),
    # (3.1,   95),
    (3.2 , 100),
]

def mapVoltageToPercent(v, mapping=mapping_20C):
    i = 0
    while True:
        point = mapping[i]
        if v < point[0]:
            break
        i += 1
        if i == len(mapping):
            return mapping[-1][1]
    if i == 0:
        return mapping[0][1]
    p1 = mapping[i - 1]
    p2 = mapping[i]
    return p1[1] + (v - p1[0]) * (p2[1] - p1[1]) / (p2[0] - p1[0])


def mapVoltageToPercentTempCompensated(v, t):
    if t < 20:
        percentage20 = mapVoltageToPercent(v, mapping_20C)
        percentage0 = mapVoltageToPercent(v, mapping_0C)
        return (t - 0) / (20 - 0) * (percentage20 - percentage0) + percentage0
    else:
        percentage60 = mapVoltageToPercent(v, mapping_60C)
        percentage20 = mapVoltageToPercent(v, mapping_20C)
        return (t - 20) / (60 - 20) * (percentage60 - percentage20) + percentage20


def mapCurve(voltages, t):
    # return np.array(list(map(mapVoltageToPercent, voltages)))
    return np.array(list(map(lambda v: mapVoltageToPercentTempCompensated(v, t), voltages)))


class Mode(Enum):
    Init = enum.auto()
    Unsupported = enum.auto()
    MoveTo = enum.auto()
    LineTo = enum.auto()
    LineToHoriz = enum.auto()
    LineToVert = enum.auto()

    def toChar(self, rel=False) -> str:
        c = {
            Mode.Init: '<init>',
            Mode.Unsupported: '<unsupported>',
            Mode.MoveTo: 'M',
            Mode.LineTo: 'L',
            Mode.LineToHoriz: 'H',
            Mode.LineToVert: 'V'
        }[self]
        if rel:
            c = c.lower()
        return c


def svgPathToXY(svgPath, startPos=(0,0)):
    coordinates = []

    mode = Mode.Init
    rel = False
    pos = list(startPos)
    prevAdded = False

    for part in svgPath.split():
        if part[0].isnumeric() or part[0] in '-.':
            params = list(map(float, part.split(',')))

            prevPos = tuple(pos)
            addPrev = False
            add = False

            # print(mode.toChar(rel), ','.join(map(str, params)), '-> ', end='')
            
            if mode == Mode.Init:
                raise RuntimeError('Coordinate before mode')
            elif mode == Mode.Unsupported:
                pass
            elif mode == Mode.MoveTo:
                if rel:
                    pos[0] += params[0]
                    pos[1] += params[1]
                else:
                    pos[0] = params[0]
                    pos[1] = params[1]
                # print('move to', pos)
                add = False

                # Following values are interpreted as implicit LineTo commands
                mode = Mode.LineTo
            elif mode == Mode.LineTo:
                if params != [0, 0]:
                    if rel:
                        pos[0] += params[0]
                        pos[1] += params[1]
                    else:
                        pos[0] = params[0]
                        pos[1] = params[1]
                    addPrev = True
                    add = True
                # print('line to', pos)
            elif mode == Mode.LineToHoriz:
                if params[0] != 0:
                    if rel:
                        pos[0] += params[0]
                    else:
                        pos[0] = params[0]
                    addPrev = True
                    add = True
                # print('line to', pos)
            elif mode == Mode.LineToVert:
                if params[0] != 0:
                    if rel:
                        pos[1] += params[0]
                    else:
                        pos[1] = params[0]
                    addPrev = True
                    add = True
                # print('line to', pos)
            else:
                raise NotImplementedError
            if addPrev:
                coordinates.append(prevPos)
            if add:
                coordinates.append(tuple(pos))
            prevAdded = add
        else:
            try:
                mode, rel = {
                    'M': (Mode.MoveTo, False),
                    'm': (Mode.MoveTo, True),
                    'L': (Mode.LineTo, False),
                    'l': (Mode.LineTo, True),
                    'H': (Mode.LineToHoriz, False),
                    'h': (Mode.LineToHoriz, True),
                    'V': (Mode.LineToVert, False),
                    'v': (Mode.LineToVert, True),
                }[part]
            except KeyError:
                print(f'Unsupported command: \'{part}\'')
                mode = Mode.Unsupported

    return coordinates


def importCurve(config: Config):
    svgPath = Path(config.filename).read_text()

    data = np.array(svgPathToXY(svgPath))

    data_x = data[:,0]
    data_y = data[:,1]

    # SVG path commands might not be in order, therefore we sort them
    sort_indices = data_x.argsort()
    data_x = data_x[sort_indices]
    data_y = data_y[sort_indices]

    # Normalize X coordinates
    x_min = np.min(data_x)
    x_max = np.max(data_x)
    data_x = config.range_x[0] + (data_x - x_min) * (config.range_x[1] - config.range_x[0]) / (x_max - x_min)

    # Normalize Y coordinates
    y_min = np.min(data_y)
    y_max = np.max(data_y)
    data_y = config.range_y[0] + (data_y - y_min) * (config.range_y[1] - config.range_y[0]) / (y_max - y_min)

    return data_x, data_y


def plotCurve(data_x, data_y, config: Config):
    plt.plot(data_x, data_y, label=config.note_text)


def plotCurveInverse(data_x, data_y, config: Config):
    plt.plot(data_y, config.range_x[1] - data_x, label=config.note_text)


def importAndPlotAll(configs):
    datas = list(map(importCurve, configs))

    plt.figure(figsize=(8, 6), dpi=100)

    for i, config in enumerate(configs):
        data = datas[i]
        plotCurve(data[0], data[1], config)

    plt.ylabel('Battery Voltage in V')
    plt.xlabel('Discharge %')
    plt.title('CR123A Voltage vs. Capacity %')
    plt.grid()
    plt.legend()
    plt.tight_layout()

    plt.savefig('plot_v_vs_percent.png')

    plt.figure(figsize=(8, 6), dpi=100)

    for i, config in enumerate(configs):
        data = datas[i]
        plotCurveInverse(data[0], data[1], config)

    plt.ylabel('Remaining Capacity in %')
    plt.xlabel('Battery Voltage in V')
    plt.title('CR123A Capacity % vs. Voltage')
    plt.grid()
    plt.legend()
    plt.tight_layout()

    plt.savefig('plot_percent_vs_v.png')

    plt.figure(figsize=(8, 6), dpi=100)

    for i, config in enumerate(configs):
        data = datas[i]
        yMapped = mapCurve(data[1], config.temp)
        plotCurve(config.range_x[1] - data[0], yMapped, config)

    x_ideal = [0, 100]
    y_ideal = [0, 100]
    plt.plot(x_ideal, y_ideal, '--', label='Ideal')

    plt.ylabel('Reported Remaining %')
    plt.xlabel('Actual Remaining %')
    plt.title('Actual vs. Reported Discharge %, Temp. Compensated')
    plt.grid()
    plt.legend()
    plt.tight_layout()

    plt.savefig('plot_percentage_mapping.png')

    return datas


if __name__ == '__main__':
    importAndPlotAll([
        config_Maxell_5mA,
        config_GP_10mA,
        config_GP_30mA,
        config_Maxell_40mA,
        config_Maxell_40mA_60C,
        config_Maxell_40mA_0C,
        config_Maxell_40mA__10C,
    ])

    plt.show()
